USE [WhitePortal]
GO


/*		assumption - a real world product table would contain many more attributes than this table, but those elements are not needed for this exercise  */
 
IF NOT EXISTS
(
    SELECT *
    FROM [sys].[objects]
    WHERE [object_id] = OBJECT_ID(N'[dbo].[Product]')
          AND [type] IN ( N'U' )
)
BEGIN
    CREATE TABLE [dbo].[Product]
    (
        [ProductID] INT NOT NULL,
        [ProductName] VARCHAR(256) NOT NULL,
        [ProductType] VARCHAR(1) NOT NULL CONSTRAINT CK_ProductType CHECK ([ProductType] IN ('F','P')) ,
        CONSTRAINT [PK_Product]
            PRIMARY KEY CLUSTERED (ProductID ASC)
           
    ) ON [PRIMARY];
END;
GO


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND name = N'IX_ProductNameType')
CREATE UNIQUE NONCLUSTERED INDEX [IX_ProductNameType] ON [dbo].[Product]
(
	[ProductName] ASC,
	[ProductType] ASC
)
GO





