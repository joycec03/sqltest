USE [WhitePortal]
GO

INSERT INTO [dbo].[Client]
           ([ClientID]
           ,[ClientName])
     VALUES
           (1,'First Client'),
           (2,'Second Client'),
           (3,'Third Client')
GO



INSERT INTO [dbo].[Customer]
           ([CustomerID]
           ,[CustomerName]
           ,[ClientID]
		   ,[ClientCustomerID])
     VALUES
           (1,'Customer 1',1,'C1A001'),
           (2,'Customer 2',1,'C1A002'),
           (3,'Customer 3',2,'20'),
           (4,'Customer 4',2,'40'),
           (5,'Customer 5',3,'6000-A'),
           (6,'Customer 6',3,'7000-B')
GO


INSERT INTO [dbo].[Product]
           ([ProductID]
           ,[ProductName]
           ,[ProductType])
     VALUES
           (1,'Equifax - 3B','F'),
           (2,'Equifax - 3B','P'),
           (3,'Experian - 3B','F'),
           (4,'Experian - 3B','P'),
           (5,'TransUnion - 3B','F'),
           (6,'TransUnion - 3B','P')
GO

INSERT INTO [dbo].[Sales]
           ([ProductID]
           ,[ClientID]
           ,[CustomerID]
           ,[SalesDate]
           ,[Quantity])
     VALUES 
	(1,1,1,'2022-01-27',10),
	(2,1,2,'2022-01-27',20),
	(5,1,1,'2021-12-15',50),
	(3,2,3,'2022-01-27',10),
	(4,2,4,'2022-01-27',20),
	(6,2,4,'2022-01-20',10),
	(5,3,5,'2021-12-20',60),
	(6,3,6,'2021-12-27',10),
	(6,3,5,'2022-01-10',50),
	(6,3,5,'2022-01-27',70),
	(5,1,1,'2021-12-15',10),
	(6,2,4,'2022-01-20',20)
  GO
 
  