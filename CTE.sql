USE [WhitePortal]
GO

DECLARE @ThirtyDaysAgo DATE ;
SET @ThirtyDaysAgo = DATEADD(dd,-30,GETDATE()) ;

WITH [Reports30Days] AS (
	SELECT     [Client].[ClientName],
	SUM( [Sales].[Quantity] ) AS [TotalReportsLast30Days]
	FROM [Sales]
	JOIN [dbo].[Client] ON [Client].[ClientID] = [Sales].[ClientID]
	WHERE [Sales].[SalesDate] > @ThirtyDaysAgo
	GROUP BY  [Client].[ClientName]
)
SELECT [Reports30Days].[ClientName],    [TotalReportsLast30Days]
FROM [Reports30Days] WHERE [Reports30Days].[TotalReportsLast30Days] > 100
ORDER BY [Reports30Days].[ClientName] ;