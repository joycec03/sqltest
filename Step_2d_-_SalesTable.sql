USE [WhitePortal]
GO

 
/*		assumption - a real world sales table would likely contain many more attributes than this table, but those elements are not needed for this exercise  */

IF NOT EXISTS
(
    SELECT *
    FROM [sys].[objects]
    WHERE [object_id] = OBJECT_ID(N'[dbo].[Sales]')
          AND [type] IN ( N'U' )
)
BEGIN
    CREATE TABLE [dbo].[Sales]
    (
        [SalesID] BIGINT  IDENTITY(1,1)  NOT NULL,
        [ProductID] INT NOT NULL,
		[ClientID] INT NOT NULL,
		[CustomerID] INT NOT NULL,
		[SalesDate] [DATE] NOT NULL,
		[Quantity] INT NOT NULL
        CONSTRAINT [PK_Sales]
            PRIMARY KEY CLUSTERED (SalesID ASC)
            
    ) ON [PRIMARY];
END;
GO

/*		Foreign Keys		*/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sales_Client]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sales]'))
ALTER TABLE [dbo].[Sales]  WITH CHECK ADD  CONSTRAINT [FK_Sales_Client] FOREIGN KEY([ClientID])
REFERENCES [dbo].[Client] ([ClientID])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sales_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sales]'))
ALTER TABLE [dbo].[Sales]  WITH CHECK ADD  CONSTRAINT [FK_Sales_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sales_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sales]'))
ALTER TABLE [dbo].[Sales]  WITH CHECK ADD  CONSTRAINT [FK_Sales_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO

 