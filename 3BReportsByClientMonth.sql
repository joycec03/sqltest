USE [WhitePortal]
GO

/*		assumption - basic query needed as input to reporting tool which will handle report format and filtering  */

SELECT     [Client].[ClientName],
CONVERT(CHAR (7),[Sales].[SalesDate]) AS [SalesMonth],
SUM( [Sales].[Quantity] ) AS [TransUnion 3B Reports]
FROM [Sales]
JOIN [dbo].[Client] ON [Client].[ClientID] = [Sales].[ClientID]
JOIN [dbo].[Product] ON [Product].[ProductID] = [Sales].[ProductID] AND [Product].[ProductName] = 'TransUnion - 3B'
GROUP BY CONVERT(CHAR(7), [Sales].[SalesDate]), [Client].[ClientName]
ORDER BY [Client].[ClientName],[SalesMonth] ;

 
 
 
 