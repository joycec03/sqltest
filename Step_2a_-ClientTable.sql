USE [WhitePortal]
GO


/*		assumption - a real world client table would contain many more attributes than this table, but those elements are not needed for this exercise  */

IF NOT EXISTS
(
    SELECT *
    FROM [sys].[objects]
    WHERE [object_id] = OBJECT_ID(N'[dbo].[Client]')
          AND [type] IN ( N'U' )
)
BEGIN
    CREATE TABLE [dbo].[Client]
    (
        [ClientID] INT NOT NULL,
        [ClientName] VARCHAR(256) NOT NULL,
        CONSTRAINT [PK_Client]
            PRIMARY KEY CLUSTERED (ClientID ASC)
                 ) ON [PRIMARY]
END;
GO


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Client]') AND name = N'IX_ClientName')
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientName] ON [dbo].[Client]
(
	[ClientName] ASC
)
GO

