USE [WhitePortal]
GO

/*		assumption 1 - we generate reports/data for our clients who then distribute them to their customers	 */
/*		assumption 2 - a real world customer table would contain many more attributes than this table, but those elements are not needed for this exercise  */

IF NOT EXISTS
(
    SELECT *
    FROM [sys].[objects]
    WHERE [object_id] = OBJECT_ID(N'[dbo].[Customer]')
          AND [type] IN ( N'U' )
)
BEGIN
    CREATE TABLE [dbo].[Customer]
    (
        [CustomerID] INT NOT NULL,
        [CustomerName] VARCHAR(256) NOT NULL,
		[ClientID] INT NOT NULL,
		[ClientCustomerID] VARCHAR(32) NOT NULL,
        CONSTRAINT [PK_Customer]
            PRIMARY KEY CLUSTERED (CustomerID ASC)
            
    ) ON [PRIMARY];
END;
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Customer_Client]') AND parent_object_id = OBJECT_ID(N'[dbo].[Customer]'))
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Client] FOREIGN KEY([ClientID])
REFERENCES [dbo].[Client] ([ClientID])
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND name = N'IX_Customer_Name')
CREATE NONCLUSTERED INDEX [IX_Customer_Name] ON [dbo].[Customer]
(
	[CustomerName] ASC
)
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND name = N'IX_Customer_Client')
CREATE NONCLUSTERED INDEX [IX_Customer_Client] ON [dbo].[Customer]
(
	[CustomerID] ASC
)
GO

